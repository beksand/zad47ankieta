package com.beksand;

public class Question {
    private int numberAnswer;
    private String question;

    public Question(int numberAnswer, String question) {
        this.numberAnswer = numberAnswer;
        this.question = question;
    }

    public int getNumberAnswer() {
        return numberAnswer;
    }

    public void setNumberAnswer(int numberAnswer) {
        this.numberAnswer = numberAnswer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
    public boolean checkNumberAnswer(int answer){
        if (answer==getNumberAnswer()){
            return true;
        } else {
            return false;
        }
    }
}
