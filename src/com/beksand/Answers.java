package com.beksand;

import java.util.ArrayList;
import java.util.List;


public class Answers {
    private List<String> answers = new ArrayList<>();

    public Answers(List<String> answers) {
        this.answers = answers;
    }
    public void addAnswers(String answer){
        answers.add(answer);
    }
}
